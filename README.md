# JetBrains config

This project is used by JetBrains IDEs to sync settings

## Plugins

### IntelliJ IDEA
- [Git Commit Message Helper](https://plugins.jetbrains.com/plugin/13477-git-commit-message-helper)
- [GitToolBox](https://plugins.jetbrains.com/plugin/7499-gittoolbox)